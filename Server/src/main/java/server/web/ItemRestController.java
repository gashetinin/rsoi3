package server.web;

import org.springframework.http.*;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import server.web.model.request.ItemRequest;
import server.web.model.response.ItemResponse;
import server.web.model.response.ListOfItemResponces;

import javax.servlet.http.HttpServletResponse;
import java.net.URI;

/**
 * Created by grigory on 11/30/16.
 */
@RestController
@RequestMapping("/item")
public class ItemRestController {
    private final String addItemUrl = "http://localhost:8083/item";
    private final String getItemUrl = "http://localhost:8083/item/";
    private final String getAllItemUrl = "http://localhost:8083/item/";
    private final String getItemPageUrl = "http://localhost:8083/item/";
    private final String updateItemPageUrl = "http://localhost:8083/item/";
    private final String deleteItemPageUrl = "http://localhost:8083/item/";


    private final String checkTokenUrl = "http://localhost:8086/user/checkToken";


    @RequestMapping(value="/{id}", method = RequestMethod.GET)
    public ItemResponse getItem(@PathVariable Integer id) {
        RestTemplate restTemplate = new RestTemplate();
        URI uri = UriComponentsBuilder.fromUriString(getItemUrl + id).build().toUri();

        HttpHeaders headers = new HttpHeaders();
        //headers.add("Authorization",header);

        HttpEntity httpEntity = new HttpEntity<>(null, headers);

        ResponseEntity<ItemResponse> response = restTemplate.exchange(uri, HttpMethod.GET, httpEntity, ItemResponse.class);
        return response.getBody();
    }

    @RequestMapping(method = RequestMethod.GET)
    public ListOfItemResponces getAll() {
        RestTemplate restTemplate = new RestTemplate();
        URI uri = UriComponentsBuilder.fromUriString(getAllItemUrl).build().toUri();

        HttpHeaders headers = new HttpHeaders();
        //headers.add("Authorization",header);

        HttpEntity httpEntity = new HttpEntity<>(null, headers);

        ResponseEntity<ListOfItemResponces> response = restTemplate.exchange(uri, HttpMethod.GET, httpEntity, ListOfItemResponces.class);
        return response.getBody();
    }

    @RequestMapping(value = "/{startIndex}/{finishIndex}", method = RequestMethod.GET)
    public ListOfItemResponces getAllPagination(@PathVariable Integer startIndex, @PathVariable Integer finishIndex) {
        RestTemplate restTemplate = new RestTemplate();
        URI uri = UriComponentsBuilder.fromUriString(getItemPageUrl + startIndex + "/" + finishIndex).build().toUri();

        HttpHeaders headers = new HttpHeaders();
        //headers.add("Authorization",header);

        HttpEntity httpEntity = new HttpEntity<>(null, headers);

        ResponseEntity<ListOfItemResponces> response = restTemplate.exchange(uri, HttpMethod.GET, httpEntity, ListOfItemResponces.class);
        return response.getBody();
    }


    @RequestMapping(method = RequestMethod.POST)
    public Long createItem(HttpEntity<ItemRequest> fullItemRequest, HttpServletResponse response) {
        ItemRequest itemRequest = fullItemRequest.getBody();
        HttpHeaders header = fullItemRequest.getHeaders();
        String authHeader = header.get("Authorization").toString();
        String accesstoken = authHeader.substring(authHeader.indexOf('[')+1,authHeader.lastIndexOf(']'));

        RestTemplate restTemplate = new RestTemplate();
        URI uri = UriComponentsBuilder.fromUriString(checkTokenUrl).build().toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Token",accesstoken);
        HttpEntity httpEntity = new HttpEntity<>(null, headers);

        ResponseEntity<Boolean> responseFlag = restTemplate.exchange(uri, HttpMethod.GET, httpEntity, Boolean.class);

        if (responseFlag.getBody()) {

            RestTemplate restTemplateItem = new RestTemplate();
            URI uriItem = UriComponentsBuilder.fromUriString(addItemUrl).build().toUri();
            HttpEntity<ItemRequest> httpEntityItem = new HttpEntity<>(itemRequest, null);

            ResponseEntity<Long> responseItem = restTemplateItem.exchange(uriItem, HttpMethod.POST, httpEntityItem, Long.class);
            response.setStatus(HttpStatus.CREATED.value());
            return responseItem.getBody();
        }
        else
        {
            response.setStatus(401);//HttpStatus.UNAUTHORIZED.value());
            return 0L;
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public Long updateItem(@PathVariable Integer id, HttpEntity<ItemRequest> fullItemRequest, HttpServletResponse response) {
        ItemRequest itemRequest = fullItemRequest.getBody();
        HttpHeaders header = fullItemRequest.getHeaders();
        String authHeader = header.get("Authorization").toString();
        String accesstoken = authHeader.substring(authHeader.indexOf('[')+1,authHeader.lastIndexOf(']'));

        RestTemplate restTemplate = new RestTemplate();
        URI uri = UriComponentsBuilder.fromUriString(checkTokenUrl).build().toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Token",accesstoken);
        HttpEntity httpEntity = new HttpEntity<>(null, headers);

        ResponseEntity<Boolean> responseFlag = restTemplate.exchange(uri, HttpMethod.GET, httpEntity, Boolean.class);

        if (responseFlag.getBody()) {

            RestTemplate restTemplateItem = new RestTemplate();
            URI uriItem = UriComponentsBuilder.fromUriString(updateItemPageUrl+id).build().toUri();
            HttpEntity httpEntityItem = new HttpEntity<>(itemRequest, null);

            ResponseEntity<Long> responseItem = restTemplateItem.exchange(uriItem, HttpMethod.PUT, httpEntityItem, Long.class);
            response.setStatus(HttpStatus.CREATED.value());
            return responseItem.getBody();
        }
        else
        {
            response.setStatus(401);//HttpStatus.UNAUTHORIZED.value());
            return 0L;
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteItem(@PathVariable Integer id, HttpEntity<ItemRequest> fullItemRequest, HttpServletResponse response) {
        HttpHeaders header = fullItemRequest.getHeaders();
        String authHeader = header.get("Authorization").toString();
        String accesstoken = authHeader.substring(authHeader.indexOf('[')+1,authHeader.lastIndexOf(']'));

        RestTemplate restTemplate = new RestTemplate();
        URI uri = UriComponentsBuilder.fromUriString(checkTokenUrl).build().toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Token",accesstoken);
        HttpEntity httpEntity = new HttpEntity<>(null, headers);

        ResponseEntity<Boolean> responseFlag = restTemplate.exchange(uri, HttpMethod.GET, httpEntity, Boolean.class);

        if (responseFlag.getBody()) {

            RestTemplate restTemplateItem = new RestTemplate();
            URI uriItem = UriComponentsBuilder.fromUriString(deleteItemPageUrl+id).build().toUri();
            HttpEntity httpEntityItem = new HttpEntity<>(null, null);

            restTemplateItem.exchange(uriItem, HttpMethod.DELETE, httpEntityItem, Long.class);
            response.setStatus(HttpStatus.NO_CONTENT.value());
            return;
        }
        else
        {
            response.setStatus(401);//HttpStatus.UNAUTHORIZED.value());
            return;
        }
    }
}
