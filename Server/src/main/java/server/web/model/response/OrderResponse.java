package server.web.model.response;

import com.google.common.base.MoreObjects;

import java.util.List;

/**
 * Created by grigory on 11/30/16.
 */
public class OrderResponse {
    private Integer totalPrice;
    private List<ItemResponse> items;

    public OrderResponse() {
    }

    public OrderResponse(OrderResponse order) {
        this.totalPrice = order.getTotalPrice();
        if (!order.getItems().isEmpty())
            for (ItemResponse item: order.getItems()) {
                items.add(item);
            }
    }

    public OrderResponse(OrderResponseSimple order) {
        this.totalPrice = order.getTotalPrice();
    }

    public Integer getTotalPrice() {
        return totalPrice;
    }

    public List<ItemResponse> getItems() {
        return items;
    }

    public void addItemToOrder(ItemResponse itemResponse) {
        if (itemResponse != null)
            items.add(itemResponse);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("totalPrice", totalPrice)
                .add("items", items)
                .toString();
    }
}