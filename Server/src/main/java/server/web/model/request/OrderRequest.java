package server.web.model.request;

import com.google.common.base.MoreObjects;

/**
 * Created by grigory on 11/28/16.
 */
public class OrderRequest {
    private Integer totalPrice;

    public Integer getTotalPrice() {
        return totalPrice;
    }

    public OrderRequest() {
        totalPrice = 0;
    }

    public OrderRequest(OrderRequest orderRequest) {
        this.totalPrice = orderRequest.getTotalPrice();
    }



    public OrderRequest(Integer totalPrice) {
        this.totalPrice = totalPrice;
    }

    public OrderRequest setTotalPrice(Integer totalPrice) {
        this.totalPrice = totalPrice;
        return this;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("totalPrice", totalPrice)
                .toString();
    }
}

