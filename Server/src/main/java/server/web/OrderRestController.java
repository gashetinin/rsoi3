package server.web;

import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import server.web.model.request.OrderRequest;
import server.web.model.response.ListOfOrderResponces;
import server.web.model.response.OrderResponse;

import javax.servlet.http.HttpServletResponse;
import java.net.URI;

/**
 * Created by grigory on 11/30/16.
 */
@RestController
@RequestMapping("/order")
public class OrderRestController {
    private final String addOrderUrl = "http://localhost:8084/order";
    private final String getOrderUrl = "http://localhost:8089/orderItemAggregation/";
    private final String getAllOrderUrl = "http://localhost:8089/orderItemAggregation/";
    private final String getOrderPageUrl = "http://localhost:8089/orderItemAggregation/";
    private final String updateOrderPageUrl = "http://localhost:8084/order/";
    private final String deleteOrderPageUrl = "http://localhost:8084/order/";
    private final String addItemToOrderUrl = "http://localhost:8084/order/";
    private final String deleteItemFromOrderUrl = "http://localhost:8084/order/";




    private final String checkTokenUrl = "http://localhost:8086/user/checkToken";



    @RequestMapping(value="/{id}", method = RequestMethod.GET)
    public OrderResponse getOrder(@PathVariable Long id) {
        RestTemplate restTemplate = new RestTemplate();
        URI uri = UriComponentsBuilder.fromUriString(getOrderUrl + id).build().toUri();

        HttpHeaders headers = new HttpHeaders();
        //headers.add("Authorization",header);

        HttpEntity httpEntity = new HttpEntity<>(null, headers);

        ResponseEntity<OrderResponse> response = restTemplate.exchange(uri, HttpMethod.GET, httpEntity, OrderResponse.class);
        OrderResponse orderResponse = response.getBody();
        return orderResponse;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ListOfOrderResponces getAll() {
        RestTemplate restTemplate = new RestTemplate();
        URI uri = UriComponentsBuilder.fromUriString(getAllOrderUrl).build().toUri();

        HttpHeaders headers = new HttpHeaders();
        //headers.add("Authorization",header);

        HttpEntity httpEntity = new HttpEntity<>(null, headers);

        ResponseEntity<ListOfOrderResponces> response = restTemplate.exchange(uri, HttpMethod.GET, httpEntity, ListOfOrderResponces.class);
        return response.getBody();
    }

    @RequestMapping(value = "/{startIndex}/{finishIndex}", method = RequestMethod.GET)
    public ListOfOrderResponces getAllPagination(@PathVariable Integer startIndex, @PathVariable Integer finishIndex) {
        RestTemplate restTemplate = new RestTemplate();
        URI uri = UriComponentsBuilder.fromUriString(getOrderPageUrl + startIndex + "/" + finishIndex).build().toUri();

        HttpHeaders headers = new HttpHeaders();
        //headers.add("Authorization",header);

        HttpEntity httpEntity = new HttpEntity<>(null, headers);

        ResponseEntity<ListOfOrderResponces> response = restTemplate.exchange(uri, HttpMethod.GET, httpEntity, ListOfOrderResponces.class);
        return response.getBody();
    }

    @RequestMapping(method = RequestMethod.POST)
    public Long createOrder(HttpEntity<OrderRequest> fullOrderRequest, HttpServletResponse response) {
        OrderRequest orderRequest = fullOrderRequest.getBody();
        HttpHeaders header = fullOrderRequest.getHeaders();
        String authHeader = header.get("Authorization").toString();
        String accesstoken = authHeader.substring(authHeader.indexOf('[')+1,authHeader.lastIndexOf(']'));

        RestTemplate restTemplate = new RestTemplate();
        URI uri = UriComponentsBuilder.fromUriString(checkTokenUrl).build().toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Token",accesstoken);
        HttpEntity httpEntity = new HttpEntity<>(null, headers);

        ResponseEntity<Boolean> responseFlag = restTemplate.exchange(uri, HttpMethod.GET, httpEntity, Boolean.class);

        if (responseFlag.getBody()) {

            RestTemplate restTemplateOrder = new RestTemplate();
            URI uriOrder = UriComponentsBuilder.fromUriString(addOrderUrl).build().toUri();
            HttpEntity<OrderRequest> httpEntityItem = new HttpEntity<>(orderRequest, null);

            ResponseEntity<Long> responseOrder = restTemplateOrder.exchange(uriOrder, HttpMethod.POST, httpEntityItem, Long.class);
            response.setStatus(HttpStatus.CREATED.value());
            return responseOrder.getBody();
        }
        else
        {
            response.setStatus(401);//HttpStatus.UNAUTHORIZED.value());
            return 0L;
        }

    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PATCH)
    public Long updateOrder(@PathVariable Long id, HttpEntity<OrderRequest> fullOrderRequest, HttpServletResponse response) {
        OrderRequest orderRequest = fullOrderRequest.getBody();
        HttpHeaders header = fullOrderRequest.getHeaders();
        String authHeader = header.get("Authorization").toString();
        String accesstoken = authHeader.substring(authHeader.indexOf('[')+1,authHeader.lastIndexOf(']'));

        RestTemplate restTemplate = new RestTemplate();
        URI uri = UriComponentsBuilder.fromUriString(checkTokenUrl).build().toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Token",accesstoken);
        HttpEntity httpEntity = new HttpEntity<>(null, headers);

        ResponseEntity<Boolean> responseFlag = restTemplate.exchange(uri, HttpMethod.GET, httpEntity, Boolean.class);

        if (responseFlag.getBody()) {

            RestTemplate restTemplateOrder = new RestTemplate();
            URI uriOrder = UriComponentsBuilder.fromUriString(updateOrderPageUrl).build().toUri();
            HttpEntity<OrderRequest> httpEntityItem = new HttpEntity<>(orderRequest, null);

            ResponseEntity<Long> responseOrder = restTemplateOrder.exchange(uriOrder, HttpMethod.PUT, httpEntityItem, Long.class);
            response.setStatus(HttpStatus.CREATED.value());
            return responseOrder.getBody();
        }
        else
        {
            response.setStatus(401);//HttpStatus.UNAUTHORIZED.value());
            return 0L;
        }
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteOrder(@PathVariable Long id, HttpEntity<OrderRequest> fullOrderRequest, HttpServletResponse response) {
        OrderRequest orderRequest = fullOrderRequest.getBody();
        HttpHeaders header = fullOrderRequest.getHeaders();
        String authHeader = header.get("Authorization").toString();
        String accesstoken = authHeader.substring(authHeader.indexOf('[')+1,authHeader.lastIndexOf(']'));

        RestTemplate restTemplate = new RestTemplate();
        URI uri = UriComponentsBuilder.fromUriString(checkTokenUrl).build().toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Token",accesstoken);
        HttpEntity httpEntity = new HttpEntity<>(null, headers);

        ResponseEntity<Boolean> responseFlag = restTemplate.exchange(uri, HttpMethod.GET, httpEntity, Boolean.class);

        if (responseFlag.getBody()) {

            RestTemplate restTemplateOrder = new RestTemplate();
            URI uriOrder = UriComponentsBuilder.fromUriString(deleteOrderPageUrl+id).build().toUri();
            HttpEntity httpEntityItem = new HttpEntity<>(null, null);

            restTemplateOrder.exchange(uriOrder, HttpMethod.DELETE, httpEntityItem, Long.class);
            response.setStatus(HttpStatus.NO_CONTENT.value());
            return;
        }
        else
        {
            response.setStatus(401);//HttpStatus.UNAUTHORIZED.value());
            return;
        }
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/{orderId}/Item/{itemId}", method = RequestMethod.GET)
    public Long addItemToOrder(@PathVariable Long orderId, @PathVariable Long itemId, HttpEntity<OrderRequest> fullOrderRequest, HttpServletResponse response) {
        HttpHeaders header = fullOrderRequest.getHeaders();
        String authHeader = header.get("Authorization").toString();
        String accesstoken = authHeader.substring(authHeader.indexOf('[')+1,authHeader.lastIndexOf(']'));

        RestTemplate restTemplate = new RestTemplate();
        URI uri = UriComponentsBuilder.fromUriString(checkTokenUrl).build().toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Token",accesstoken);
        HttpEntity httpEntity = new HttpEntity<>(null, headers);

        ResponseEntity<Boolean> responseFlag = restTemplate.exchange(uri, HttpMethod.GET, httpEntity, Boolean.class);

        if (responseFlag.getBody()) {
            RestTemplate restTemplateOrder = new RestTemplate();
            URI uriOrder = UriComponentsBuilder.fromUriString(addItemToOrderUrl+orderId+"/Item/"+itemId).build().toUri();
            HttpEntity httpEntityItem = new HttpEntity<>(null, null);

            restTemplateOrder.exchange(uriOrder, HttpMethod.POST, httpEntityItem, Long.class);
            response.setStatus(HttpStatus.CREATED.value());
            return orderId;
        }
        else
        {
            response.setStatus(401);//HttpStatus.UNAUTHORIZED.value());
            return orderId;
        }
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/{orderId}/Item/{itemId}", method = RequestMethod.DELETE)
    public Long deleteItemFromOrder(@PathVariable Long orderId, @PathVariable Long itemId, HttpEntity<OrderRequest> fullOrderRequest, HttpServletResponse response) {
        HttpHeaders header = fullOrderRequest.getHeaders();
        String authHeader = header.get("Authorization").toString();
        String accesstoken = authHeader.substring(authHeader.indexOf('[')+1,authHeader.lastIndexOf(']'));

        RestTemplate restTemplate = new RestTemplate();
        URI uri = UriComponentsBuilder.fromUriString(checkTokenUrl).build().toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Token",accesstoken);
        HttpEntity httpEntity = new HttpEntity<>(null, headers);

        ResponseEntity<Boolean> responseFlag = restTemplate.exchange(uri, HttpMethod.GET, httpEntity, Boolean.class);

        if (responseFlag.getBody()) {

            RestTemplate restTemplateOrder = new RestTemplate();
            URI uriOrder = UriComponentsBuilder.fromUriString(deleteItemFromOrderUrl+orderId+"/Item/"+itemId).build().toUri();
            HttpEntity httpEntityItem = new HttpEntity<>(null, null);

            restTemplateOrder.exchange(uriOrder, HttpMethod.DELETE, httpEntityItem, Long.class);
            response.setStatus(HttpStatus.CREATED.value());
            return orderId;
        }
        else
        {
            response.setStatus(401);//HttpStatus.UNAUTHORIZED.value());
            return orderId;
        }
    }
}
