package client.service;

/**
 * Created by grigory on 12/13/16.
 */
public interface AuthorizeAlgorithm {

    String getAuthorizeUrl();

    boolean requestAccessToken(String authorizationCode);

    boolean refreshToken();

    String getAccessToken();

    String getCsrf();

}
