package client.service;

import client.web.model.Requests.ItemRequest;
import client.web.model.Requests.OrderRequest;
import client.web.model.Response.ItemResponse;
import client.web.model.Response.ListOfItemResponces;
import client.web.model.Response.ListOfOrderResponces;
import client.web.model.Response.OrderResponse;

/**
 * Created by grigory on 12/11/16.
 */
public interface DBAccess {

    public String addItem(ItemRequest itemRequest, String accessToken);

    public ItemResponse getItem(Long itemId);

    public String deleteItem(Long itemId, String accessToken);

    public String updateItem(Long itemId, ItemRequest itemRequest, String accessToken);

    public ListOfItemResponces getAllItems();

    public ListOfItemResponces getItems(int startIndex, int finishIndex);


    public String addOrder(OrderRequest orderRequest, String accessToken);

    public OrderResponse getOrder(Long orderId);

    public String deleteOrder(Long orderId, String accessToken);

    public String updateOrder(Long orderId, OrderRequest orderRequest, String accessToken);

    public ListOfOrderResponces getAllOrders();

    public ListOfOrderResponces getOrders(int startIndex, int finishIndex);

    public String addItemToOrder(Long orderId, Long itemId, String accessToken);

    public String deleteItemFromOrder(Long orderId, Long itemId, String accessToken);


}
