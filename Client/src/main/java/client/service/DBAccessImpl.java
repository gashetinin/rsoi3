package client.service;

import client.web.model.Requests.ItemRequest;
import client.web.model.Requests.OrderRequest;
import client.web.model.Response.ItemResponse;
import client.web.model.Response.ListOfItemResponces;
import client.web.model.Response.ListOfOrderResponces;
import client.web.model.Response.OrderResponse;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

/**
 * Created by grigory on 12/11/16.
 */
@Service
public class DBAccessImpl implements DBAccess {

    @Override
    public String addItem (ItemRequest itemRequest, String accessToken) {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization",accessToken);

        HttpEntity<ItemRequest> requestUpdate = new HttpEntity<ItemRequest>(itemRequest,headers);
        ResponseEntity<Long> response = restTemplate.exchange("http://localhost:8082/item", HttpMethod.POST, requestUpdate, Long.class);
        if (response.getStatusCodeValue() == 401) {
            return "Refresh token required";
        }
        Long itemId = response.getBody();
        return "Item added: id="+itemId.toString();
    }

    @Override
    public ItemResponse getItem(Long itemId) {
        RestTemplate restTemplate = new RestTemplate();
        String address = "http://localhost:8082/item/"+ Long.toString(itemId);
        URI uri = UriComponentsBuilder.fromUriString(address).build().toUri();
        ItemResponse itemResponse = new ItemResponse(restTemplate.getForObject(uri, ItemResponse.class));
        return itemResponse;
    }

    @Override
    public String deleteItem(Long itemId, String accessToken) {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization",accessToken);

        String address = "http://localhost:8082/item/"+ Long.toString(itemId);
        URI uri = UriComponentsBuilder.fromUriString(address).build().toUri();

        HttpEntity<ItemRequest> requestDelete = new HttpEntity<ItemRequest>(null,headers);
        ResponseEntity<Long> response = restTemplate.exchange(uri, HttpMethod.DELETE, requestDelete, Long.class);
        if (response.getStatusCodeValue() == 401) {
            return "Refresh token required";
        }
        return "Deleted";
    }

    @Override
    public String updateItem(Long itemId, ItemRequest itemRequest, String accessToken) {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization",accessToken);

        String address = "http://localhost:8082/item/"+ Long.toString(itemId);
        URI uri = UriComponentsBuilder.fromUriString(address).build().toUri();

        HttpEntity<ItemRequest> requestUpdate = new HttpEntity<ItemRequest>(itemRequest,headers);
        ResponseEntity<Long> response = restTemplate.exchange(uri, HttpMethod.PUT, requestUpdate, Long.class);
        if (response.getStatusCodeValue() == 401) {
            return "Refresh token required";
        }
        itemId = response.getBody();
        return "Item updated: id="+itemId.toString();
    }

    @Override
    public ListOfItemResponces getAllItems() {
        RestTemplate restTemplate = new RestTemplate();
        String address = "http://localhost:8082/item/";
        URI uri = UriComponentsBuilder.fromUriString(address).build().toUri();
        ResponseEntity<ListOfItemResponces> response = restTemplate.getForEntity(uri, ListOfItemResponces.class);
        return response.getBody();
    }

    @Override
    public ListOfItemResponces getItems(int startIndex, int finishIndex) {
        RestTemplate restTemplate = new RestTemplate();
        String address = "http://localhost:8082/item/" + startIndex + "/" + finishIndex;
        URI uri = UriComponentsBuilder.fromUriString(address).build().toUri();
        ResponseEntity<ListOfItemResponces> response = restTemplate.getForEntity(uri, ListOfItemResponces.class);
        return response.getBody();
    }

    @Override
    public String addOrder(OrderRequest orderRequest, String accessToken) {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization",accessToken);

        HttpEntity<OrderRequest> requestUpdate = new HttpEntity<>(orderRequest,headers);
        ResponseEntity<Long> response = restTemplate.exchange("http://localhost:8082/order", HttpMethod.POST, requestUpdate, Long.class);
        if (response.getStatusCodeValue() == 401) {
            return "Refresh token required";
        }
        Long orderId = response.getBody();
        return "Order added: id="+orderId.toString();
    }

    @Override
    public OrderResponse getOrder(Long orderId) {
        RestTemplate restTemplate = new RestTemplate();
        String address = "http://localhost:8082/order/"+ Long.toString(orderId);
        URI uri = UriComponentsBuilder.fromUriString(address).build().toUri();
        OrderResponse orderResponse = restTemplate.getForObject(uri, OrderResponse.class);
        return orderResponse;
    }

    @Override
    public String deleteOrder(Long orderId, String accessToken) {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization",accessToken);

        String address = "http://localhost:8082/order/"+ Long.toString(orderId);
        URI uri = UriComponentsBuilder.fromUriString(address).build().toUri();

        HttpEntity<ItemRequest> requestDelete = new HttpEntity<ItemRequest>(null,headers);
        ResponseEntity<Long> response = restTemplate.exchange(uri, HttpMethod.DELETE, requestDelete, Long.class);
        if (response.getStatusCodeValue() == 401) {
            return "Refresh token required";
        }
        return "Deleted";
    }

    @Override
    public String updateOrder(Long orderId, OrderRequest orderRequest, String accessToken) {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization",accessToken);

        String address = "http://localhost:8082/order/"+ Long.toString(orderId);
        URI uri = UriComponentsBuilder.fromUriString(address).build().toUri();

        HttpEntity<OrderRequest> requestUpdate = new HttpEntity<>(orderRequest,headers);
        ResponseEntity<Long> response = restTemplate.exchange(uri, HttpMethod.PUT, requestUpdate, Long.class);
        if (response.getStatusCodeValue() == 401) {
            return "Refresh token required";
        }
        orderId = response.getBody();
        return "Item updated: id="+orderId.toString();
    }

    @Override
    public ListOfOrderResponces getAllOrders() {
        RestTemplate restTemplate = new RestTemplate();
        String address = "http://localhost:8082/order/";
        URI uri = UriComponentsBuilder.fromUriString(address).build().toUri();
        ResponseEntity<ListOfOrderResponces> response = restTemplate.getForEntity(uri, ListOfOrderResponces.class);
        return response.getBody();
    }

    @Override
    public ListOfOrderResponces getOrders(int startIndex, int finishIndex) {
        RestTemplate restTemplate = new RestTemplate();
        String address = "http://localhost:8082/order/" + startIndex + "/" + finishIndex;
        URI uri = UriComponentsBuilder.fromUriString(address).build().toUri();
        ResponseEntity<ListOfOrderResponces> response = restTemplate.getForEntity(uri, ListOfOrderResponces.class);
        return response.getBody();
    }

    @Override
    public String addItemToOrder(Long orderId, Long itemId, String accessToken) {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization",accessToken);

        String address = "http://localhost:8082/order/" + orderId + "/Item/" + itemId;
        URI uri = UriComponentsBuilder.fromUriString(address).build().toUri();

        HttpEntity<OrderRequest> requestAddItemToOrder = new HttpEntity<OrderRequest>(null,headers);
        ResponseEntity<Long> response = restTemplate.exchange(uri, HttpMethod.POST, requestAddItemToOrder, Long.class);
        if (response.getStatusCodeValue() == 401) {
            return "Refresh token required";
        }
        return "Added";
    }

    @Override
    public String deleteItemFromOrder(Long orderId, Long itemId, String accessToken) {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization",accessToken);

        String address = "http://localhost:8082/order/" + orderId + "/Item/" + itemId;
        URI uri = UriComponentsBuilder.fromUriString(address).build().toUri();

        HttpEntity<OrderRequest> requestAddItemToOrder = new HttpEntity<OrderRequest>(null,headers);
        ResponseEntity<Long> response = restTemplate.exchange(uri, HttpMethod.DELETE, requestAddItemToOrder, Long.class);
        if (response.getStatusCodeValue() == 401) {
            return "Refresh token required";
        }
        return "Added";
    }
}
