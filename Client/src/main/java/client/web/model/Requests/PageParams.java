package client.web.model.Requests;

/**
 * Created by grigory on 12/13/16.
 */
public class PageParams {
    private int startIndex;
    private int finishIndex;

    public PageParams() {
        this.startIndex = 0;
        this.finishIndex = 0;
    }

    public PageParams(int startIndex, int finishIndex) {
        this.startIndex = startIndex;
        this.finishIndex = finishIndex;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public PageParams setStartIndex(int startIndex) {
        this.startIndex = startIndex;
        return this;
    }

    public int getFinishIndex() {
        return finishIndex;
    }

    public PageParams setFinishIndex(int finishIndex) {
        this.finishIndex = finishIndex;
        return this;
    }
}
