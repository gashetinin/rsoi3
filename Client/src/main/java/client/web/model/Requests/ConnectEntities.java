package client.web.model.Requests;

import com.google.common.base.MoreObjects;

/**
 * Created by grigory on 12/20/16.
 */
public class ConnectEntities {
    private Long firstEntityId;
    private Long secondEntityId;

    public ConnectEntities() {
        this.firstEntityId = 0L;
        this.secondEntityId = 0L;
    }

    public ConnectEntities(Long firstEntityId, Long secondEntityId) {
        this.firstEntityId = firstEntityId;
        this.secondEntityId = secondEntityId;
    }

    public ConnectEntities(ConnectEntities connectEntities) {
        this.firstEntityId = connectEntities.getFirstEntityId();
        this.secondEntityId = connectEntities.getSecondEntityId();
    }

    public Long getFirstEntityId() {
        return firstEntityId;
    }

    public ConnectEntities setFirstEntityId(Long firstEntityId) {
        this.firstEntityId = firstEntityId;
        return this;
    }

    public Long getSecondEntityId() {
        return secondEntityId;
    }

    public ConnectEntities setSecondEntityId(Long secondEntityId) {
        this.secondEntityId = secondEntityId;
        return this;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("firstEntityId", firstEntityId)
                .add("secondEntityId", secondEntityId)
                .toString();
    }
}
