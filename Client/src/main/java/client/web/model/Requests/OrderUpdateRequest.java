package client.web.model.Requests;

import com.google.common.base.MoreObjects;

/**
 * Created by grigory on 12/20/16.
 */
public class OrderUpdateRequest extends OrderRequest {
    private Long orderId;

    public OrderUpdateRequest() {
        super();
        this.orderId = 0L;
    }

    public OrderUpdateRequest(OrderRequest orderRequest, Long orderId) {
        super(orderRequest);
        this.orderId = orderId;
    }

    public OrderUpdateRequest(Integer totalPrice, Long orderId) {
        super(totalPrice);
        this.orderId = orderId;
    }

    public Long getOrderId() {
        return orderId;
    }

    public OrderUpdateRequest setOrderId(Long orderId) {
        this.orderId = orderId;
        return this;
    }


    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("orderId", orderId)
                .add("totalPrice", getTotalPrice())
                .toString();
    }


}
