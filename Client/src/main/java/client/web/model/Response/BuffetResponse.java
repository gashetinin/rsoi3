package client.web.model.Response;

import com.google.common.base.MoreObjects;

import java.util.List;

/**
 * Created by grigory on 11/29/16.
 */
public class BuffetResponse {
    private String name;
    private String location;
    private List<OrderResponse> orders;
    private List<ItemResponse> items;

    public BuffetResponse(BuffetResponse buffet) {
        this.name = buffet.getName();
        this.location = buffet.getLocation();
        if (!buffet.getOrders().isEmpty())
            for (OrderResponse order: buffet.getOrders()) {
                orders.add(order);
            }

        if (!buffet.getItems().isEmpty())
            for (ItemResponse item: buffet.getItems()) {
                items.add(item);
            }
            
    }

    public String getName() {
        return name;
    }

    public String getLocation() {
        return location;
    }

    public List<OrderResponse> getOrders() {
        return orders;
    }

    public List<ItemResponse> getItems() {
        return items;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("name", name)
                .add("location", location)
                .add("orders", orders)
                .add("items", items)
                .toString();
    }
}
