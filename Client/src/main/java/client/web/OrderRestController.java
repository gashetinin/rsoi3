package client.web;

import client.service.AuthorizeAlgorithm;
import client.service.DBAccess;
import client.web.model.Requests.*;
import client.web.model.Response.ListOfOrderResponces;
import client.web.model.Response.OrderResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * Created by grigory on 11/30/16.
 */
@RestController
@RequestMapping("/order")
public class OrderRestController {
    @Autowired
    private DBAccess dbAccess;

    @Autowired
    private AuthorizeAlgorithm authorizeAlgorithm;

    @RequestMapping(value = "/addOrder", method = RequestMethod.GET)
    public ModelAndView addOrderPage() {
        return new ModelAndView("addOrderPage");
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/addOrder", method = RequestMethod.POST)
    public String createOrder(OrderRequest orderRequest) {
        String result = dbAccess.addOrder(orderRequest, authorizeAlgorithm.getAccessToken());
        return result;
    }

    @RequestMapping(value = "/getOrder", method = RequestMethod.GET)
    public ModelAndView getOrderPage() {
        return new ModelAndView("getOrderPage");
    }

    @RequestMapping(value="/getOrder", method = RequestMethod.POST)
    public OrderResponse getOrder(Integer id) {
        return dbAccess.getOrder(Integer.toUnsignedLong(id));
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<OrderResponse> getAll() {
        ListOfOrderResponces listOfOrderResponces= dbAccess.getAllOrders();
        if (listOfOrderResponces != null)
            return listOfOrderResponces.getOrderResponses();
        return null;
    }

    @RequestMapping(value="/getPage", method = RequestMethod.GET)
    public ModelAndView getAllPagination() {
        return new ModelAndView("getOrderPaginationPage");
    }

    @RequestMapping(value="/getPage", method = RequestMethod.POST)
    public List<OrderResponse> getAllPagination(PageParams pageParams) {
        ListOfOrderResponces listOfOrderResponces= dbAccess.getOrders(pageParams.getStartIndex(),pageParams.getFinishIndex());
        if (listOfOrderResponces != null)
            return listOfOrderResponces.getOrderResponses();
        return null;
    }

    @RequestMapping(value = "/updateOrder", method = RequestMethod.GET)
    public ModelAndView updateOrder() {
        return new ModelAndView("updateOrderPage");
    }

    @RequestMapping(value = "/updateOrder", method = RequestMethod.POST)
    public String updateOrder(OrderUpdateRequest orderUpdateRequest) {
        return dbAccess.updateOrder(orderUpdateRequest.getOrderId(), new OrderRequest(orderUpdateRequest.getTotalPrice()), authorizeAlgorithm.getAccessToken());
    }

    @RequestMapping(value = "/deleteOrder", method = RequestMethod.GET)
    public ModelAndView deleteOrder() {
        return new ModelAndView("deleteOrderPage");
    }

    @RequestMapping(value = "/deleteOrder", method = RequestMethod.POST)
    public String deleteOrder(Integer id) {
        dbAccess.deleteOrder(Integer.toUnsignedLong(id), authorizeAlgorithm.getAccessToken());
        return "Order deleted!";
    }

    @RequestMapping(value = "/addItemToOrder", method = RequestMethod.GET)
    public ModelAndView addItemToOrder() {
        return new ModelAndView("addItemToOrderPage");
    }

    @RequestMapping(value = "/addItemToOrder", method = RequestMethod.POST)
    public String addItemToOrder(ConnectEntities connectEntities) {
        dbAccess.addItemToOrder(connectEntities.getFirstEntityId(),connectEntities.getSecondEntityId(), authorizeAlgorithm.getAccessToken());
        return "Item added!";
    }

    @RequestMapping(value = "/deleteItemFromOrder", method = RequestMethod.GET)
    public ModelAndView deleteItemFromOrder() {
        return new ModelAndView("deleteItemFromOrderPage");
    }

    @RequestMapping(value = "/deleteItemFromOrder", method = RequestMethod.POST)
    public String deleteItemFromOrder(ConnectEntities connectEntities) {
        dbAccess.deleteItemFromOrder(connectEntities.getFirstEntityId(),connectEntities.getSecondEntityId(), authorizeAlgorithm.getAccessToken());
        return "Item deleted!";
    }


}
