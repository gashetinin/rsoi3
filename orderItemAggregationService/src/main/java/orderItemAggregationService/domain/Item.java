package orderItemAggregationService.domain;

import com.google.common.base.MoreObjects;

/**
 * Created by grigory on 11/27/16.
 */
public class Item {
    private  Long id;
    private String name;
    private Integer price;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public Item setName(String name) {
        this.name = name;
        return this;
    }

    public Item setPrice(int price) {
        this.price = price;
        return this;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("name", name)
                .add("price", price)
                .toString();
    }

    public Item(String name, Integer price) {
        this.name = name;
        this.price = price;
    }

    public Item() {
        this.name = "";
        this.price = 0;
    }

}
