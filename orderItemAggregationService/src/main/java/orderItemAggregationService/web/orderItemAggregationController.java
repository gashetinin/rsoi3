package orderItemAggregationService.web;

import orderItemAggregationService.web.model.response.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

/**
 * Created by grigory on 12/21/16.
 */
@RestController
@RequestMapping("/orderItemAggregation")
public class orderItemAggregationController {

    private final String orderUri = "http://localhost:8084/order/";
    private final String itemUri = "http://localhost:8083/item/";

    @RequestMapping(value="/{orderId}", method = RequestMethod.GET)
    public OrderResponse getOrder(@PathVariable Integer orderId) {
        RestTemplate restTemplate = new RestTemplate();
        String address = orderUri + Long.toString(orderId);
        URI uri = UriComponentsBuilder.fromUriString(address).build().toUri();
        OrderResponseSimple orderResponseSimple = restTemplate.getForObject(uri, OrderResponseSimple.class);
        OrderResponse orderResponse = new OrderResponse(orderResponseSimple);

        if (orderResponseSimple.getItems() != null)
            for (Long itemId: orderResponseSimple.getItems()) {
                String addressItem = itemUri+ Long.toString(itemId);
                URI uriItem = UriComponentsBuilder.fromUriString(addressItem).build().toUri();
                orderResponse.addItemToOrder(restTemplate.getForObject(uriItem, ItemResponse.class));
            }
        return orderResponse;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ListOfOrderResponces getAll() {
        RestTemplate restTemplate = new RestTemplate();
        String address = orderUri;
        URI uri = UriComponentsBuilder.fromUriString(address).build().toUri();
        ResponseEntity<ListOfOrderResponcesSimple> response = restTemplate.getForEntity(uri, ListOfOrderResponcesSimple.class);
        ListOfOrderResponcesSimple listOfOrderResponcesSimple = response.getBody();
        ListOfOrderResponces listOfOrderResponces = new ListOfOrderResponces();

        for (OrderResponseSimple orderResponseSimple : listOfOrderResponcesSimple.getOrderResponsesSimple()) {
            OrderResponse orderResponse = new OrderResponse(orderResponseSimple);
            for (Long itemId : orderResponseSimple.getItems()) {
                String addressItem = itemUri + Long.toString(itemId);
                URI uriItem = UriComponentsBuilder.fromUriString(addressItem).build().toUri();
                orderResponse.addItemToOrder(restTemplate.getForObject(uriItem, ItemResponse.class));
            }
            listOfOrderResponces.addOrderResponse(orderResponse);
        }
        return listOfOrderResponces;
    }

    @RequestMapping(value = "/{startIndex}/{finishIndex}", method = RequestMethod.GET)
    public ListOfOrderResponces getAllPagination(@PathVariable Integer startIndex, @PathVariable Integer finishIndex) {
        RestTemplate restTemplate = new RestTemplate();
        String address = orderUri+startIndex+"/"+finishIndex;
        URI uri = UriComponentsBuilder.fromUriString(address).build().toUri();
        ResponseEntity<ListOfOrderResponcesSimple> response = restTemplate.getForEntity(uri, ListOfOrderResponcesSimple.class);
        ListOfOrderResponcesSimple listOfOrderResponcesSimple = response.getBody();
        ListOfOrderResponces listOfOrderResponces = new ListOfOrderResponces();

        for (OrderResponseSimple orderResponseSimple : listOfOrderResponcesSimple.getOrderResponsesSimple()) {
            OrderResponse orderResponse = new OrderResponse(orderResponseSimple);
            for (Long itemId : orderResponseSimple.getItems()) {
                String addressItem = itemUri + Long.toString(itemId);
                URI uriItem = UriComponentsBuilder.fromUriString(addressItem).build().toUri();
                orderResponse.addItemToOrder(restTemplate.getForObject(uriItem, ItemResponse.class));
            }
            listOfOrderResponces.addOrderResponse(orderResponse);
        }
        return listOfOrderResponces;
    }
}