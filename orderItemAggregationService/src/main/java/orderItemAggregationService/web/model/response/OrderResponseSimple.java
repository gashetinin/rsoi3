package orderItemAggregationService.web.model.response;

import com.google.common.base.MoreObjects;
import orderItemAggregationService.domain.Order;

import java.util.List;

/**
 * Created by grigory on 11/30/16.
 */
public class OrderResponseSimple {
    private Integer totalPrice;
    private List<Long> items;
    private Long buffet;
    private Long user;

    public OrderResponseSimple() {
        totalPrice = 0;
        buffet = 0L;
        user = 0L;
    }

    public OrderResponseSimple(Order order) {
        this.totalPrice = order.getTotalPrice();
        if (!order.getItems().isEmpty())
            for (Long item: order.getItems()) {
                items.add(item);
            }
        if (order.getBuffet() != null)
            this.buffet = order.getBuffet();
        if (order.getUser() != null)
            this.user = order.getUser();
    }


    public OrderResponseSimple(OrderResponseSimple order) {
        this.totalPrice = order.getTotalPrice();
        if (!order.getItems().isEmpty())
            for (Long item: order.getItems()) {
                items.add(item);
            }
        if (order.getBuffet() != null)
            this.buffet = order.getBuffet();
        if (order.getUser() != null)
            this.user = order.getUser();
    }

    public Integer getTotalPrice() {
        return totalPrice;
    }

    public List<Long> getItems() {
        return items;
    }

    public Long getBuffet() {
        return buffet;
    }

    public Long getUser() {
        return user;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("totalPrice", totalPrice)
                .add("items", items)
                .add("buffet", buffet)
                .add("user", user)
                .toString();
    }
}
