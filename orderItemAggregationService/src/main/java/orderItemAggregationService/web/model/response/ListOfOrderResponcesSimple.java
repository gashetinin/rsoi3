package orderItemAggregationService.web.model.response;

import java.util.List;

/**
 * Created by grigory on 12/12/16.
 */
public class ListOfOrderResponcesSimple {

    private List<OrderResponseSimple> orderResponsesSimple;

    public ListOfOrderResponcesSimple() {
        this.orderResponsesSimple = null;
    }

    public ListOfOrderResponcesSimple(List<OrderResponseSimple> orderResponses) {
        this.orderResponsesSimple = orderResponses;
    }

    public List<OrderResponseSimple> getOrderResponsesSimple() {
        return orderResponsesSimple;
    }

    public ListOfOrderResponcesSimple setOrderResponsesSimple(List<OrderResponseSimple> orderResponses) {
        this.orderResponsesSimple = orderResponses;
        return this;
    }
}
