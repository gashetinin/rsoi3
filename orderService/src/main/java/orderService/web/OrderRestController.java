package orderService.web;

import orderService.domain.Order;
import orderService.service.OrderService;
import orderService.web.model.Request.OrderRequest;
import orderService.web.model.Response.ListOfOrderResponces;
import orderService.web.model.Response.OrderResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by grigory on 11/30/16.
 */
@RestController
@RequestMapping("/order")
public class OrderRestController {
    @Autowired
    private OrderService orderService;

    @RequestMapping(value="/{id}", method = RequestMethod.GET)
    public OrderResponse getOrder(@PathVariable Long id) {
        Order temp = orderService.getById(id);
        return new OrderResponse(temp);
    }

    @RequestMapping(method = RequestMethod.GET)
    public ListOfOrderResponces getAll() {
        return new ListOfOrderResponces(orderService.getAll().stream().map(OrderResponse::new).collect(Collectors.toList()));
    }

    @RequestMapping(value = "/{startIndex}/{finishIndex}", method = RequestMethod.GET)
    public ListOfOrderResponces getAllPagination(@PathVariable Integer startIndex, @PathVariable Integer finishIndex) {
        PageRequest pageRequest = new PageRequest(startIndex - 1, finishIndex-startIndex);
        List<OrderResponse> itemResponses = orderService.getAllPagination(pageRequest).stream().map(OrderResponse::new).collect(Collectors.toList());
        return new ListOfOrderResponces(itemResponses);
    }

    @RequestMapping(method = RequestMethod.POST)
    public Long createOrder(HttpEntity<OrderRequest> orderRequestEntity, HttpServletResponse response) {
        OrderRequest orderRequest = orderRequestEntity.getBody();
        Order order = orderService.save(orderRequest);
        response.setStatus(HttpStatus.CREATED.value());
        return order.getId();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PATCH)
    public Long updateOrder(@PathVariable Long id, OrderRequest orderRequest, HttpServletResponse response) {
        Order order = orderService.update(id, orderRequest);
        response.setStatus(HttpStatus.CREATED.value());
        return order.getId();
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteOrder(@PathVariable Long id, OrderRequest orderRequest, HttpServletResponse response) {
        orderService.delete(id);
        response.setStatus(HttpStatus.NO_CONTENT.value());
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/{orderId}/Item/{itemId}", method = RequestMethod.GET)
    public void addItemToOrder(@PathVariable Long orderId, @PathVariable Long itemId, HttpServletResponse response) {
        Long updatedOrderId = orderService.addItemToOrder(orderId,itemId);
        response.addHeader(HttpHeaders.LOCATION,"/order/"+updatedOrderId);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/{orderId}/Item/{itemId}", method = RequestMethod.DELETE)
    public void deleteItemFromOrder(@PathVariable Long orderId, @PathVariable Long itemId, HttpServletResponse response) {
        Long updatedOrderId = orderService.delItemFromOrder(orderId,itemId);
        response.addHeader(HttpHeaders.LOCATION,"/order/"+updatedOrderId);
    }
}
