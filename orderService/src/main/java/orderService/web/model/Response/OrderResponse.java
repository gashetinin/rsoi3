package orderService.web.model.Response;

import com.google.common.base.MoreObjects;
import orderService.domain.Order;

import java.util.List;

/**
 * Created by grigory on 11/30/16.
 */
public class OrderResponse {
    private Integer totalPrice;
    private List<Long> items;
    private Long buffet;
    private Long user;

    public OrderResponse() {
        totalPrice = 0;
        buffet = 0L;
        user = 0L;
    }

    public OrderResponse(Order order) {
        this.totalPrice = order.getTotalPrice();
        if (!order.getItems().isEmpty())
            for (Long item: order.getItems()) {
                items.add(item);
            }
        if (order.getBuffet() != null)
            this.buffet = order.getBuffet();
        if (order.getUser() != null)
            this.user = order.getUser();
    }


    public OrderResponse(OrderResponse order) {
        this.totalPrice = order.getTotalPrice();
        if (!order.getItems().isEmpty())
            for (Long item: order.getItems()) {
                items.add(item);
            }
        if (order.getBuffet() != null)
            this.buffet = order.getBuffet();
        if (order.getUser() != null)
            this.user = order.getUser();
    }

    public Integer getTotalPrice() {
        return totalPrice;
    }

    public List<Long> getItems() {
        return items;
    }

    public Long getBuffet() {
        return buffet;
    }

    public Long getUser() {
        return user;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("totalPrice", totalPrice)
                .add("items", items)
                .add("buffet", buffet)
                .add("user", user)
                .toString();
    }
}
