package orderService.web.model.Request;

import com.google.common.base.MoreObjects;

import java.util.List;

/**
 * Created by grigory on 11/28/16.
 */
public class OrderRequest {
    private Integer totalPrice;
    private List<Long> items;

    public Integer getTotalPrice() {
        return totalPrice;
    }

    public OrderRequest setTotalPrice(Integer totalPrice) {
        this.totalPrice = totalPrice;
        return this;
    }

    public List<Long> getItems() {
        return items;
    }

    public OrderRequest setItems(List<Long> items) {
        this.items = items;
        return this;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("totalPrice", totalPrice)
                .add("items", items)
                .toString();
    }
}
