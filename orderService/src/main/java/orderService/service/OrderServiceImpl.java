package orderService.service;

import orderService.domain.Order;
import orderService.repository.OrderRepository;
import orderService.web.model.Request.OrderRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.List;

import static org.springframework.data.jpa.domain.AbstractPersistable_.id;

/**
 * Created by grigory on 11/28/16.
 */

@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderRepository orderRepository;

    @Override
    @Transactional(readOnly = true)
    public Order getById(Long id) {
        Order order = orderRepository.findOne(id);
        if (order == null) {
            throw new EntityNotFoundException("Order '{" + id + "}' not found");
        }
        return order;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Order> getAll() {
        return orderRepository.findAll();
    }

    @Override
    public List<Order> getAllPagination(PageRequest pageRequest) {
        Page<Order> orderPage = orderRepository.findAll(pageRequest);
        return orderPage.getContent();
    }

    @Override
    @Transactional
    public Order save(OrderRequest orderRequest) {
        Order order = new Order(orderRequest.getTotalPrice(),orderRequest.getItems());
        return orderRepository.save(order);
    }

    @Override
    @Transactional
    public Order save(Order order) {
        return orderRepository.save(order);
    }

    @Override
    @Transactional
    public Order update(Long id, OrderRequest orderRequest) {
        Order order = orderRepository.findOne(id);
        if (order == null) {
            throw new EntityNotFoundException("Order '{" + id + "}' not found");
        }

        order.setTotalPrice(orderRequest.getTotalPrice() != null ? orderRequest.getTotalPrice() : order.getTotalPrice());
        order.setItems(orderRequest.getItems());

        return orderRepository.save(order);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        orderRepository.delete(id);
    }

    @Override
    public Long addItemToOrder(Long orderId, Long itemId) {
        Order order = orderRepository.findOne(orderId);
        if (order == null) {
            throw new EntityNotFoundException("Order '{" + id + "}' not found");
        }

        order.addItem(itemId);
        orderRepository.save(order);
        return order.getId();
    }

    @Override
    public Long delItemFromOrder(Long orderId, Long itemId) {
        Order order = orderRepository.findOne(orderId);
        if (order == null) {
            throw new EntityNotFoundException("Order '{" + id + "}' not found");
        }

        order.delItem(itemId);
        orderRepository.save(order);
        return order.getId();
    }

}
