package orderService.service;


import orderService.domain.Order;
import orderService.web.model.Request.OrderRequest;
import org.springframework.data.domain.PageRequest;

import java.util.List;

/**
 * Created by grigory on 11/28/16.
 */
public interface OrderService {

    Order getById(Long id);

    List<Order> getAll();

    List<Order> getAllPagination(PageRequest pageRequest);

    Order save(OrderRequest orderRequest);
    Order save(Order order);

    Order update(Long id, OrderRequest orderRequest);

    void delete(Long id);

    Long addItemToOrder(Long orderId, Long itemId);

    Long delItemFromOrder(Long orderId, Long itemId);
}
