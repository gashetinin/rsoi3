package orderService.domain;

import com.google.common.base.MoreObjects;

import javax.persistence.*;
import java.util.List;

/**
 * Created by grigory on 11/27/16.
 */
@Entity
@Table(name = "RSOI3_order")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="total_price")
    private Integer totalPrice;

    @Column
    @ElementCollection(targetClass=Long.class)
    private List<Long> items;

    @Column
    private Long buffetId;

    @Column
    private Long userId;

    public Long getId() {
        return id;
    }

    public Integer getTotalPrice() {
        return totalPrice;
    }

    public List<Long> getItems() {
        return items;
    }

    public Long getBuffet() {
        return buffetId;
    }

    public Long getUser() {
        return userId;
    }

    public Order setTotalPrice(Integer totalPrice) {
        this.totalPrice = totalPrice;
        return this;
    }

    public Order setItems(List<Long> items) {
        this.items = items;
        return this;
    }

    public Order setBuffet(Long buffet) {
        this.buffetId = buffet;
        return this;
    }

    public Order setUser(Long user) {
        this.userId = user;
        return this;
    }

    public Order addItem(Long item) {
        this.items.add(item);
        return this;
    }

    public Order delItem(Long item) {
        this.items.remove(item);
        return this;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("totalPrice", totalPrice)
                .add("items", items)
                .add("buffet", buffetId)
                .add("user", userId)
                .toString();
    }

    public Order(Integer totalPrice, List<Long> items) {
        this.totalPrice = totalPrice;
        this.items = items;
    }

    public Order() {
        this.totalPrice = 0;
        this.items = null;
    }

}
