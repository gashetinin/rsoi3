package orderService.repository;

import orderService.domain.Order;
import org.springframework.data.jpa.repository.JpaRepository;


/**
 * Created by grigory on 11/27/16.
 */
public interface OrderRepository extends JpaRepository<Order,Long> {
}