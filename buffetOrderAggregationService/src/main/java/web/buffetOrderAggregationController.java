package web;


import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import web.response.*;

import java.net.URI;

/**
 * Created by grigory on 12/21/16.
 */
@RestController
@RequestMapping("/buffetOrderAggregation")
public class buffetOrderAggregationController {

    private final String buffetUri = "http://localhost:8085/buffet/";
    private final String orderUri = "http://localhost:8089/orderItemAggregation/";
    private final String itemUri = "http://localhost:8083/item/";

    @RequestMapping(value="/{id}", method = RequestMethod.GET)
    public BuffetResponse getUser(@PathVariable Long userId) {
        RestTemplate restTemplate = new RestTemplate();
        String address = buffetUri + Long.toString(userId);
        URI uri = UriComponentsBuilder.fromUriString(address).build().toUri();
        BuffetResponseSimple buffetResponseSimple = new BuffetResponseSimple(restTemplate.getForObject(uri, BuffetResponseSimple.class));
        BuffetResponse buffetResponse = new BuffetResponse(buffetResponseSimple);

        for (Long orderId: buffetResponseSimple.getOrders()) {
            String addressOrder = orderUri + Long.toString(orderId);
            URI uriOrder = UriComponentsBuilder.fromUriString(addressOrder).build().toUri();
            buffetResponse.AddOrderToBuffet(restTemplate.getForObject(uriOrder, OrderResponse.class));
        }

        for (Long itemId: buffetResponseSimple.getItems()) {
            String addressItem = itemUri + Long.toString(itemId);
            URI uriItem = UriComponentsBuilder.fromUriString(addressItem).build().toUri();
            buffetResponse.AddItemToBuffet(restTemplate.getForObject(uriItem, ItemResponse.class));
        }

        return buffetResponse;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ListOfBuffetResponces getAll() {
        RestTemplate restTemplate = new RestTemplate();
        String address = buffetUri;
        URI uri = UriComponentsBuilder.fromUriString(address).build().toUri();
        ResponseEntity<ListOfBuffetResponcesSimple> response = restTemplate.getForEntity(uri, ListOfBuffetResponcesSimple.class);
        ListOfBuffetResponcesSimple listOfBuffetResponcesSimple = response.getBody();
        ListOfBuffetResponces listOfBuffetResponces = new ListOfBuffetResponces();

        for (BuffetResponseSimple buffetResponseSimple : listOfBuffetResponcesSimple.getBuffetResponseSimples()) {
            BuffetResponse buffetResponse = new BuffetResponse(buffetResponseSimple);
            for (Long orderId : buffetResponseSimple.getOrders()) {
                String addressOrder = orderUri + Long.toString(orderId);
                URI uriOrder = UriComponentsBuilder.fromUriString(addressOrder).build().toUri();
                buffetResponse.AddOrderToBuffet(restTemplate.getForObject(uriOrder, OrderResponse.class));
            }

            for (Long itemId : buffetResponseSimple.getItems()) {
                String addressItem = orderUri + Long.toString(itemId);
                URI uriItem = UriComponentsBuilder.fromUriString(addressItem).build().toUri();
                buffetResponse.AddItemToBuffet(restTemplate.getForObject(uriItem, ItemResponse.class));
            }
            listOfBuffetResponces.addBuffetResponse(buffetResponse);
        }

        return listOfBuffetResponces;
    }
}