package web.response;


import com.google.common.base.MoreObjects;
import domain.Buffet;

import java.util.List;

/**
 * Created by grigory on 11/29/16.
 */
public class BuffetResponseSimple {
    private String name;
    private String location;
    private List<Long> orders;
    private List<Long> items;

    public BuffetResponseSimple(Buffet buffet) {
        this.name = buffet.getName();
        this.location = buffet.getLocation();
        if (!buffet.getOrders().isEmpty())
            for (Long order: buffet.getOrders()) {
                orders.add(order);
            }

        if (!buffet.getItems().isEmpty())
            for (Long item: buffet.getItems()) {
                items.add(item);
            }

    }


    public BuffetResponseSimple(BuffetResponseSimple buffet) {
        this.name = buffet.getName();
        this.location = buffet.getLocation();
        if (!buffet.getOrders().isEmpty())
            for (Long order: buffet.getOrders()) {
                orders.add(order);
            }

        if (!buffet.getItems().isEmpty())
            for (Long item: buffet.getItems()) {
                items.add(item);
            }
            
    }

    public String getName() {
        return name;
    }

    public String getLocation() {
        return location;
    }

    public List<Long> getOrders() {
        return orders;
    }

    public List<Long> getItems() {
        return items;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("name", name)
                .add("location", location)
                .add("orders", orders)
                .add("items", items)
                .toString();
    }
}
