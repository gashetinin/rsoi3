package web.response;

import java.util.List;

/**
 * Created by grigory on 12/12/16.
 */
public class ListOfBuffetResponcesSimple {

    private List<BuffetResponseSimple> buffetResponseSimples;

    public ListOfBuffetResponcesSimple() {
        this.buffetResponseSimples = null;
    }

    public ListOfBuffetResponcesSimple(List<BuffetResponseSimple> buffetResponseSimples) {
        this.buffetResponseSimples = buffetResponseSimples;
    }

    public List<BuffetResponseSimple> getBuffetResponseSimples() {
        return buffetResponseSimples;
    }

    public ListOfBuffetResponcesSimple setBuffetResponseSimples(List<BuffetResponseSimple> buffetResponseSimples) {
        this.buffetResponseSimples = buffetResponseSimples;
        return this;
    }
}
