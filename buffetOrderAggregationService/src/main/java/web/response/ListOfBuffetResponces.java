package web.response;

import java.util.List;

/**
 * Created by grigory on 12/12/16.
 */
public class ListOfBuffetResponces {

    private List<BuffetResponse> buffetResponses;

    public ListOfBuffetResponces() {
        this.buffetResponses = null;
    }

    public ListOfBuffetResponces(List<BuffetResponse> buffetResponses) {
        this.buffetResponses = buffetResponses;
    }

    public List<BuffetResponse> getBuffetResponses() {
        return buffetResponses;
    }

    public ListOfBuffetResponces setBuffetResponses(List<BuffetResponse> buffetResponses) {
        this.buffetResponses = buffetResponses;
        return this;
    }

    public void addBuffetResponse (BuffetResponse buffetResponse) {
        if (buffetResponse != null)
            this.buffetResponses.add(buffetResponse);
    }
}
