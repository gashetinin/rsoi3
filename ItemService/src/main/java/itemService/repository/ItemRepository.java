package itemService.repository;

import itemService.domain.Item;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by grigory on 11/27/16.
 */
public interface ItemRepository extends JpaRepository<Item,Long> {
}