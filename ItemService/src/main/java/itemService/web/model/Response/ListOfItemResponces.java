package itemService.web.model.Response;

import java.util.List;

/**
 * Created by grigory on 12/12/16.
 */
public class ListOfItemResponces {

    private List<ItemResponse> itemResponses;

    public ListOfItemResponces() {
        this.itemResponses = null;
    }

    public ListOfItemResponces(List<ItemResponse> itemResponses) {
        this.itemResponses = itemResponses;
    }

    public List<ItemResponse> getItemResponses() {
        return itemResponses;
    }

    public ListOfItemResponces setItemResponses(List<ItemResponse> itemResponses) {
        this.itemResponses = itemResponses;
        return this;
    }
}
