package itemService.web.model.Response;

import com.google.common.base.MoreObjects;
import itemService.domain.Item;


/**
 * Created by grigory on 11/30/16.
 */
public class ItemResponse {
    private String name;
    private Integer price;

    public ItemResponse(Item item) {
        this.name = item.getName();
        this.price = item.getPrice();
    }

    public ItemResponse() {
        this.name = "";
        this.price = 0;
    }

    public ItemResponse(ItemResponse item) {
        this.name = item.getName();
        this.price = item.getPrice();
    }

    public String getName() {
        return name;
    }

    public Integer getPrice() {
        return price;
    }

    public ItemResponse setName(String name) {
        this.name = name;
        return this;
    }

    public ItemResponse setPrice(Integer price) {
        this.price = price;
        return this;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("name", name)
                .add("price", price)
                .toString();
    }
}
