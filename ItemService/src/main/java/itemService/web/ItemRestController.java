package itemService.web;

import itemService.domain.Item;
import itemService.service.ItemService;
import itemService.web.model.Request.ItemRequest;
import itemService.web.model.Response.ItemResponse;
import itemService.web.model.Response.ListOfItemResponces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by grigory on 11/30/16.
 */
@RestController
@RequestMapping("/item")
public class ItemRestController {
    @Autowired
    private ItemService itemService;

    @RequestMapping(value="/{id}", method = RequestMethod.GET)
    public ItemResponse getItem(@PathVariable Integer id) {
        Item item = itemService.getById(Integer.toUnsignedLong(id));
        ItemResponse itemResponse = new ItemResponse();
        if (item != null) {
            itemResponse.setName(item.getName());
            itemResponse.setPrice(item.getPrice());
        }
        return itemResponse;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ListOfItemResponces getAll() {
        return new ListOfItemResponces(itemService.getAll().stream().map(ItemResponse::new).collect(Collectors.toList()));
    }

    @RequestMapping(value = "/{startIndex}/{finishIndex}", method = RequestMethod.GET)
    public ListOfItemResponces getAllPagination(@PathVariable Integer startIndex, @PathVariable Integer finishIndex) {
        PageRequest pageRequest = new PageRequest(startIndex - 1, finishIndex-startIndex);
        List<ItemResponse> itemResponses = itemService.getAllPagination(pageRequest).stream().map(ItemResponse::new).collect(Collectors.toList());
        return new ListOfItemResponces(itemResponses);
    }


    @RequestMapping(method = RequestMethod.POST)
    public Long createItem(HttpEntity<ItemRequest> itemRequest, HttpServletResponse response) {
        Item item = itemService.save(itemRequest.getBody());
        response.setStatus(HttpStatus.CREATED.value());
        return item.getId();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public Long updateItem(@PathVariable Integer id, HttpEntity<ItemRequest> itemRequest, HttpServletResponse response) {
        Item item = itemService.update(Integer.toUnsignedLong(id), itemRequest.getBody());
        response.setStatus(HttpStatus.CREATED.value());
        return item.getId();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteItem(@PathVariable Integer id, HttpServletResponse response) {
        itemService.delete(Integer.toUnsignedLong(id));
        response.setStatus(HttpStatus.NO_CONTENT.value());
    }
}
