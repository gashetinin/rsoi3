package itemService.service;

import itemService.domain.Item;
import itemService.web.model.Request.ItemRequest;
import org.springframework.data.domain.PageRequest;

import java.util.List;

/**
 * Created by grigory on 11/28/16.
 */
public interface ItemService {

    Item getById(Long id);

    List<Item> getAll();

    List<Item> getAllPagination(PageRequest pageRequest);

    Item save(ItemRequest itemRequest);
    Item save(Item item);

    Item update(Long id, ItemRequest itemRequest);

    void delete(Long id);
}
