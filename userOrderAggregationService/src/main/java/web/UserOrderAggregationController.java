package web;


import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import web.model.response.*;

import java.net.URI;

/**
 * Created by grigory on 12/21/16.
 */
@RestController
@RequestMapping("/userOrderAggregation")
public class UserOrderAggregationController {

    private final String userUri = "http://localhost:8086/user/";
    private final String orderUri = "http://localhost:8089/orderItemAggregation/";

    @RequestMapping(value="/{id}", method = RequestMethod.GET)
    public UserResponse getUser(@PathVariable Long userId) {
        RestTemplate restTemplate = new RestTemplate();
        String address = orderUri + Long.toString(userId);
        URI uri = UriComponentsBuilder.fromUriString(address).build().toUri();
        UserResponseSimple userResponseSimple = new UserResponseSimple(restTemplate.getForObject(uri, UserResponseSimple.class));
        UserResponse userResponse = new UserResponse(userResponseSimple);

        for (Long orderId: userResponseSimple.getOrders()) {
            String addressOrder = orderUri + Long.toString(orderId);
            URI uriOrder = UriComponentsBuilder.fromUriString(addressOrder).build().toUri();
            userResponse.addOrderToUser(restTemplate.getForObject(uriOrder, OrderResponse.class));
        }
        return userResponse;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ListOfUserResponces getAll() {
        RestTemplate restTemplate = new RestTemplate();
        String address = userUri;
        URI uri = UriComponentsBuilder.fromUriString(address).build().toUri();
        ResponseEntity<ListOfUserResponcesSimple> response = restTemplate.getForEntity(uri, ListOfUserResponcesSimple.class);
        ListOfUserResponcesSimple listOfUserResponcesSimple = response.getBody();
        ListOfUserResponces listOfUserResponces = new ListOfUserResponces();

        for (UserResponseSimple userResponseSimple : listOfUserResponcesSimple.getUserResponsesSimple()) {
            UserResponse userResponse = new UserResponse(userResponseSimple);
            for (Long orderId : userResponseSimple.getOrders()) {
                String addressItem = orderUri + Long.toString(orderId);
                URI uriItem = UriComponentsBuilder.fromUriString(addressItem).build().toUri();
                userResponse.addOrderToUser(restTemplate.getForObject(uriItem, OrderResponse.class));
            }
            listOfUserResponces.addUserResponse(userResponse);
        }
        return listOfUserResponces;
    }
}