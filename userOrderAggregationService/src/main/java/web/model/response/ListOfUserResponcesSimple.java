package web.model.response;

import java.util.List;

/**
 * Created by grigory on 12/12/16.
 */
public class ListOfUserResponcesSimple {

    private List<UserResponseSimple> userResponseSimples;

    public ListOfUserResponcesSimple() {
        this.userResponseSimples = null;
    }

    public ListOfUserResponcesSimple(List<UserResponseSimple> userResponseSimples) {
        this.userResponseSimples = userResponseSimples;
    }

    public List<UserResponseSimple> getUserResponsesSimple() {
        return userResponseSimples;
    }

    public ListOfUserResponcesSimple setOrderResponses(List<UserResponseSimple> userResponses) {
        this.userResponseSimples = userResponses;
        return this;
    }

    public void addUserResponseSimple(UserResponseSimple userResponseSimple) {
        if (userResponseSimple != null)
            userResponseSimples.add(userResponseSimple);
    }
}
