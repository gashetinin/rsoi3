package web.model.response;

import com.google.common.base.MoreObjects;


/**
 * Created by grigory on 11/30/16.
 */
public class ItemResponse {
    private String name;
    private Integer price;

/*    public ItemResponse(Item item) {
        this.name = item.getName();
        this.price = item.getPrice();
    }*/

    public ItemResponse(ItemResponse item) {
        this.name = item.getName();
        this.price = item.getPrice();
    }

    public String getName() {
        return name;
    }

    public Integer getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("name", name)
                .add("price", price)
                .toString();
    }
}
