package web.model.response;

import java.util.List;

/**
 * Created by grigory on 12/12/16.
 */
public class ListOfOrderResponces {

    private List<OrderResponse> orderResponses;

    public ListOfOrderResponces() {
        this.orderResponses = null;
    }

    public ListOfOrderResponces(List<OrderResponse> orderResponses) {
        this.orderResponses = orderResponses;
    }

    public List<OrderResponse> getOrderResponses() {
        return orderResponses;
    }

    public ListOfOrderResponces setOrderResponses(List<OrderResponse> orderResponses) {
        this.orderResponses = orderResponses;
        return this;
    }

    public void addOrderResponse (OrderResponse orderResponse) {
        if (orderResponse != null)
            orderResponses.add(orderResponse);
    }
}
