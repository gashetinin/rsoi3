package web.model.response;

import java.util.List;

/**
 * Created by grigory on 12/12/16.
 */
public class ListOfUserResponces {

    private List<UserResponse> userResponses;

    public ListOfUserResponces() {
        this.userResponses = null;
    }

    public ListOfUserResponces(List<UserResponse> userResponses) {
        this.userResponses = userResponses;
    }

    public List<UserResponse> getUserResponses() {
        return userResponses;
    }

    public ListOfUserResponces setOrderResponses(List<UserResponse> userResponses) {
        this.userResponses = userResponses;
        return this;
    }

    public void addUserResponse (UserResponse userResponses) {
        if (userResponses != null)
            this.userResponses.add(userResponses);
    }
}
