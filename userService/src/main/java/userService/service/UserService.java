package userService.service;

import userService.domain.User;
import userService.web.model.Request.UserRequest;

import java.util.List;

/**
 * Created by grigory on 11/28/16.
 */
public interface UserService {

    User getById(Long id);

    User findByClientId(String clientId);

    boolean checkUpToTimeToken(String token);

    List<User> getAll();

    User save(UserRequest userRequest);

    User update(Long id, UserRequest userRequest);

    void delete(Long id);

    Long addOrderToUser(Long userId, Long orderId);

    Long delOrderFromUser(Long userId, Long orderId);
}
