package buffetService.repository;

import buffetService.domain.Buffet;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by grigory on 11/27/16.
 */
public interface BuffetRepository extends JpaRepository<Buffet,Long> {
}