package buffetService.web;

import buffetService.domain.Buffet;
import buffetService.service.BuffetService;
import buffetService.web.model.Request.BuffetRequest;
import buffetService.web.model.Response.BuffetResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by grigory on 11/28/16.
 */
@RestController
@RequestMapping("/buffet")
public class BuffetRestController {

    @Autowired
    private BuffetService buffetService;

    @RequestMapping(value="/{id}", method = RequestMethod.GET)
    public BuffetResponse getBuffet(@PathVariable Long id) {
        return new BuffetResponse(buffetService.getById(id));
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<BuffetResponse> getAll() {
        return buffetService.getAll()
                .stream()
                .map(BuffetResponse::new)
                .collect(Collectors.toList());
    }

    @RequestMapping(method = RequestMethod.POST)
    public Long createBuffet(BuffetRequest buffetRequest, HttpServletResponse response) {
        Buffet buffet = buffetService.save(buffetRequest);
        response.setStatus(HttpStatus.CREATED.value());
        return buffet.getId();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PATCH)
    public Long updateBuffet(@PathVariable Long id, BuffetRequest buffetRequest, HttpServletResponse response) {
        Buffet buffet = buffetService.update(id, buffetRequest);
        response.setStatus(HttpStatus.CREATED.value());
        return buffet.getId();
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteBuffet(@PathVariable Long id, BuffetRequest buffetRequest, HttpServletResponse response) {
        buffetService.delete(id);
        response.setStatus(HttpStatus.NO_CONTENT.value());
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/{buffetId}/Item/{itemId}", method = RequestMethod.POST)
    public void addItemToBuffet(@PathVariable Long buffetId, @PathVariable Long itemId, HttpServletResponse response) {
        Long updatedBuffetId = buffetService.addItemToBuffet(buffetId,itemId);
        response.addHeader(HttpHeaders.LOCATION,"/buffet/"+updatedBuffetId);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/{buffetId}/Item/{itemId}", method = RequestMethod.DELETE)
    public void deleteItemFromBuffet(@PathVariable Long buffetId, @PathVariable Long itemId, HttpServletResponse response) {
        Long updatedBuffetId = buffetService.deleteItemFromBuffet(buffetId,itemId);
        response.addHeader(HttpHeaders.LOCATION,"/buffet/"+updatedBuffetId);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/{buffetId}/Order/{orderId}", method = RequestMethod.POST)
    public void addOrderToBuffet(@PathVariable Long buffetId, @PathVariable Long orderId, HttpServletResponse response) {
        Long updatedBuffetId = buffetService.addOrderToBuffet(buffetId,orderId);
        response.addHeader(HttpHeaders.LOCATION,"/buffet/"+updatedBuffetId);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/{buffetId}/Order/{itemId}", method = RequestMethod.DELETE)
    public void deleteOrderFromBuffet(@PathVariable Long buffetId, @PathVariable Long orderId, HttpServletResponse response) {
        Long updatedBuffetId = buffetService.deleteOrderFromBuffet(buffetId,orderId);
        response.addHeader(HttpHeaders.LOCATION,"/buffet/"+updatedBuffetId);
    }
}
