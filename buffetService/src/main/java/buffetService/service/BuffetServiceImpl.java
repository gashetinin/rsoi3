package buffetService.service;

import buffetService.domain.Buffet;
import buffetService.repository.BuffetRepository;
import buffetService.web.model.Request.BuffetRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.List;

import static org.springframework.data.jpa.domain.AbstractPersistable_.id;

/**
 * Created by grigory on 11/28/16.
 */
@Service
public class BuffetServiceImpl implements BuffetService {
    @Autowired
    private BuffetRepository buffetRepository;

    @Override
    @Transactional(readOnly = true)
    public Buffet getById(Long id) {
        Buffet buffet = buffetRepository.findOne(id);
        if (buffet == null) {
            throw new EntityNotFoundException("Buffet '{" + id + "}' not found");
        }
        return buffet;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Buffet> getAll() {
        return buffetRepository.findAll();
    }

    @Override
    @Transactional
    public Buffet save(BuffetRequest buffetRequest) {
        Buffet buffet = new Buffet(buffetRequest.getName(), buffetRequest.getLocation());
        return buffetRepository.save(buffet);
    }

    @Override
    @Transactional
    public Buffet update(Long id, BuffetRequest buffetRequest) {
        Buffet buffet = buffetRepository.findOne(id);
        if (buffet == null) {
            throw new EntityNotFoundException("Buffet '{" + id + "}' not found");
        }

        buffet.setName(buffetRequest.getName() != null ? buffetRequest.getName() : buffet.getName());
        buffet.setLocation(buffetRequest.getLocation() != null ? buffetRequest.getLocation() : buffet.getLocation());

        return buffetRepository.save(buffet);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        buffetRepository.delete(id);
    }

    @Override
    @Transactional
    public Long addItemToBuffet(Long buffetId, Long itemId) {
        Buffet buffet = buffetRepository.findOne(buffetId);
        if (buffet == null) {
            throw new EntityNotFoundException("Buffet '{" + id + "}' not found");
        }

        buffet.addItem(itemId);
        buffetRepository.save(buffet);
        return buffet.getId();
    }

    @Override
    @Transactional
    public Long deleteItemFromBuffet(Long buffetId, Long itemId) {
        Buffet buffet = buffetRepository.findOne(buffetId);
        if (buffet == null) {
            throw new EntityNotFoundException("Buffet '{" + id + "}' not found");
        }

        buffet.delItem(itemId);
        buffetRepository.save(buffet);
        return buffet.getId();
    }

    @Override
    @Transactional
    public Long addOrderToBuffet(Long buffetId, Long orderId) {
        Buffet buffet = buffetRepository.findOne(buffetId);
        if (buffet == null) {
            throw new EntityNotFoundException("Buffet '{" + id + "}' not found");
        }

        buffet.addOrder(orderId);
        buffetRepository.save(buffet);
        return buffet.getId();
    }

    @Override
    @Transactional
    public Long deleteOrderFromBuffet(Long buffetId, Long orderId) {
        Buffet buffet = buffetRepository.findOne(buffetId);
        if (buffet == null) {
            throw new EntityNotFoundException("Buffet '{" + id + "}' not found");
        }

        buffet.delOrder(orderId);
        buffetRepository.save(buffet);
        return buffet.getId();
    }
}