package buffetService.service;

import buffetService.domain.Buffet;
import buffetService.web.model.Request.BuffetRequest;

import java.util.List;

/**
 * Created by grigory on 11/28/16.
 */
public interface BuffetService {

    Buffet getById(Long id);

    List<Buffet> getAll();

    Buffet save(BuffetRequest buffetRequest);

    Buffet update(Long id, BuffetRequest buffetRequest);

    void delete(Long id);

    Long addItemToBuffet(Long buffetId, Long itemId);

    Long deleteItemFromBuffet(Long buffetId, Long itemId);

    Long addOrderToBuffet(Long buffetId, Long orderId);

    Long deleteOrderFromBuffet(Long buffetId, Long orderId);
}
