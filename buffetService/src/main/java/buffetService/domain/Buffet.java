package buffetService.domain;

import com.google.common.base.MoreObjects;

import javax.persistence.*;
import java.util.List;


/**
 * Created by grigory on 11/27/16.
 */
@Entity
@Table(name = "RSOI3_buffet")
public class Buffet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="buffet_id")
    private  Long id;

    @Column
    private String name;

    @Column
    private String location;

    @Column
    @ElementCollection(targetClass=Long.class)
    private List<Long> orders;

    @Column
    @ElementCollection(targetClass=Long.class)
    private List<Long> items;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLocation() {
        return location;
    }

    public List<Long> getOrders() {
        return orders;
    }

    public List<Long> getItems() {
        return items;
    }

    public Buffet setName(String name) {
        this.name = name;
        return this;
    }

    public Buffet setLocation(String location) {
        this.location = location;
        return this;
    }

    public Buffet setOrders(List<Long> orders) {
        this.orders = orders;
        return this;
    }

    public Buffet setItems(List<Long> items) {
        this.items = items;
        return this;
    }

    public Buffet addItem(Long item) {
        this.items.add(item);
        return this;
    }

    public Buffet delItem(Long item) {
        this.items.remove(item);
        return this;
    }

    public  Buffet addOrder(Long order) {
        this.orders.add(order);
        return this;
    }

    public  Buffet delOrder(Long order) {
        this.orders.remove(order);
        return this;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("name", name)
                .add("location", location)
                .add("orders", orders)
                .add("items", items)
                .toString();
    }

    public Buffet(String name, String location) {
        this.name = name;
        this.location = location;
        this.orders = null;
        this.items = null;
    }

    public Buffet() {
        this.name = "";
        this.location = "";
        this.orders = null;
        this.items = null;
    }

}
